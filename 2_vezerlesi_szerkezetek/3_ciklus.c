/*
============================================================================
Name        : 2_vezerlesi_szerkezetek/3_ciklus.c
Author      : Tobik János
Version     : 1.0
Title		: Ciklusok
Description	: Olyan programblokk, amely egymás után többször is lefut.
			  A ciklikusság száma az adott feltételtől függ, amely minden ciklus végrehajtása előtt eldönti, hogy újra végrehajtsa-e a blokkot.
============================================================================
*/

#include <stdio.h>

int main()
{
	
	///////////////
	// while ciklus
	///////////////

	int a = 0;
	while (a < 5)		// amíg a kisebb mint 5
	{
		printf("Az 'a' valtozo erteke: %d\n", a++);
	}



	int b;
	scanf("%d", &b);

	while (b%2 == 0)	// amíg osztható 2-vel
	{
		b/=2;			// leosztjuk 2-vel
	}
	printf("A 'b' valtozo erteke: %d\n", b);


	//////////////////
	// do-while ciklus
	//////////////////
	// A while ciklus elöltesztelős. A hátultesztelős változatához a 'do' kulcsszót használhatjuk.

	int c;
	do
	{
		scanf("%d", &c);
	} while(c <= 10);
	// Addig olvassuk be a számot, amíg nem nagyobb, mint 10.


	/////////////
	// for ciklus
	/////////////
	// Elöltesztelős ciklus, amelyhez ciklusváltozót kell használni.
	// A szintaktika tartalmazza a ciklusváltozó inicializálását és módosítását, valamint a bentmaradási feltételt.

	int i;
	for (i=0; i<10; i++)
	{
		printf("Ciklusvaltozo erteke: %d\n", i);
	}


	//////////////////
	// egymásba ágazás
	//////////////////
	// Egyszerűsítés: A blokk elhagyható ha 1 utasításból áll. (DE NEM AJÁNLOTT!)

	int j;
	for (i=0; i<10; i++)
		for (j=0; j<10; j++)
			if (i<j)
				printf("%d %d\n", i, j);
	
	

	return 0;
}