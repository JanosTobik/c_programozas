/*
============================================================================
Name        : 2_vezerlesi_szerkezetek/1_blokkok.c
Author      : Tobik János
Version     : 1.0
Title		: Blokkok szerepe a program futása közben
Description	: Lokális változók élettartama csak a blokk végéig tart.
			  Globális változók bármely blokkon belül felhasználhatóak.
============================================================================
*/

#include <stdio.h>

///////////////////
// globális változó
///////////////////

int global = 100;



int main()
{
	
	/////////////
	// létrehozás
	/////////////
	
	{
		printf("Ez egy blokk, ami {-tol }-ig tart!\n");
	}

	
	///////////////////////
	// blokkon belüli blokk
	///////////////////////
	
	{
		printf("Kulso blokk, globalis valtozo erteke: %d\n", global);
		{
			printf("Belso blokk, globalis valtozo erteke: %d\n", global);
		}
	}

	
	/////////////////////
	// blokk élettartalma
	/////////////////////

	{
		int a = 2;
		printf("%d\n", a);
	}
	// itt megszűnik az 'a' változó, blokkon kívül már nem használható
	//printf("%d\n", a);		// ez a sor fordítási hibát okoz

	
	//////////////////////
	// azonos változónevek
	//////////////////////

	int b = 3;
	{
		int b = 5;
		printf("%d\n", b);		// 'b' változó értéke: 5
	}
	printf("%d\n", b);			// 'b' változó értéke: 3

	return 0;
}