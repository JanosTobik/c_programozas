/*
============================================================================
Name        : 2_vezerlesi_szerkezetek/5_switch_case.c
Author      : Tobik János
Version     : 1.0
Title		: Switch-case
Description	: A switch-case szerkezet hasonlít az if-else if- else szerkezethez.
			  Az egyes case-ekbe szükség van a 'break' parancsra, ha azt akarjuk, hogy kilépjen a switch-case blokkból.
============================================================================
*/

#include <stdio.h>

int main()
{

	/////////////
	// létrehozás
	/////////////
	
	int valasztott = 1;

	switch(valasztott)
	{
		case 0:
		{
			printf("A valasztott szam a 0.\n");
			break;
		}
		case 1:
		{
			printf("A valasztott szam a 1.\n");
			break;
		}
		case 2:
		{
			printf("A valasztott szam a 2.\n");
			break;
		}
		default:
		{
			printf("A valasztott szam nem 0, 1, 2.\n");
		}
	}




	int number;

	printf("Irj be egy szamot: ");
	scanf("%d", &number);

	switch(number)
	{
		case 0:
		{
			printf("A beirt szam a 0.\n");
			break;
		}
		case 1:
		{
			printf("A beirt szam a 1.\n");
			break;
		}
		case 2:
		{
			printf("A beirt szam a 2.\n");
			break;
		}
		default:
		{
			printf("A beirt szam nem 0, 1, 2.\n");
		}
	}

	return 0;
}