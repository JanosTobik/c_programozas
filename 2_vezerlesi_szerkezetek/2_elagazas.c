/*
============================================================================
Name        : 2_vezerlesi_szerkezetek/2_elagazas.c
Author      : Tobik János
Version     : 1.0
Title		: Elágazások
Description	: Az elágazás olyan blokk, amely csak akkor hajtódik végre, amikor ez bizonyos feltétel teljesül.
			  Pl: egy szám páros
============================================================================
*/

#include <stdio.h>

int main()
{
	
	///////////////////////////////
	// egyszerű logikai kifejezések
	///////////////////////////////
	/*
	a == b		=>	igaz, ha 'a' és 'b' értéke egyenlő, különben hamis
	a != b		=>	igaz, ha 'a' és 'b' értéke különbözik, különben hamis
	a < b		=>	igaz, ha 'a' értéke kisebb, mint 'b' értéke, különben hamis
	a > b		=>	igaz, ha 'a' értéke nagyobb, mint 'b' értéke, különben hamis
	a <= b		=>	igaz, ha 'a' értéke kisebb vagy egyenlő, mint 'b' értéke , különben hamis
	a >= b		=>	igaz, ha 'a' értéke nagyobb vagy egyenlő, mint 'b' értéke , különben hamis
	*/


	////////////////////////////////
	// összetett logikai kifejezések
	////////////////////////////////
	/*
	(kifejezes1) && (kifejezes2)		=> ÉS
	(kifejezes1) || (kifejezes2)		=> VAGY
	(kifejezes1) ^ (kifejezes2)			=> XOR (kizáró vagy)
	!(kifejezes1)						=> TAGADÁS

	Pl: ((a>b)&&(b!=3)) || ( !((c>2)||(c<3)) )
	
	A műveleti sorrend miatt néhány zárójel elhagyható, így leegyszerűsíthető a logikai kifejezés:	(a>b && b!=3) || !(c>2 || c<3)
	*/


	////////////////////////////////
	// elágazás létrehozása
	////////////////////////////////

	// Maradékos osztás '%' jel segítségével.
	// Pl: 5%3 megadja az 5 3-mal való osztási maradékát.
	int a = 384;
	if (a%2 == 0)	// páros-e a szám
	{
		// ha páros a szám növelje az 'a' változó értékét 5-tel
		a+=5;
	}


	////////////////////////
	// ha ..., egyébként ...
	////////////////////////
	if (a%2 == 0)
	{
		a+=5;
	}
	else
	{
		a-=12;
	}


	///////////////////////////
	// ha ..., egyébként ha ...
	///////////////////////////
	// Egyszerűsítés: A blokk elhagyható ha 1 utasításból áll. (DE NEM AJÁNLOTT!)
	
	if (a%2 == 0)
		a+=5;
	else if(a%3 == 0)
		a*=2;
	else
		a-=12;


	return 0;
}