/*
============================================================================
Name        : 2_vezerlesi_szerkezetek/4_tombok.c
Author      : Tobik János
Version     : 1.0
Title		: Tömbök
Description	: Azonos típusú adatok tárolása egymás mellett.
============================================================================
*/

#include <stdio.h>

int main()
{
	
	/////////////
	// létrehozás
	/////////////
	
	int tomb[10];		// elemek típusa: int; darabszám: 10;


	//////////////////////////////
	// létrehozás és inicializálás
	//////////////////////////////

	int tomb2[5] = {5, 4, 3, 2, 1};


	/////////////////
	// elemek elérése
	/////////////////
	// A tömbben lévő elemeket külön-külön is el lehet érni, mintha egyedi változók lennének.
	// Minden elemnek van egy indexe a tömbbön belül.
	// Az első elem indexe mindig 0.

	tomb[0] = 50;
	tomb[1] = 2;
	tomb[2] = 7;

	scanf("%d", &tomb[3]);
	printf("%d\n", tomb[3]);

	// Mivel 0-tól kezdődik az indexelés, 10 elemű tömb esetén az utolsó index 9 lesz.
	//tomb[10] = 100;		// ez sor fordítási hibát okoz

	// Negatív indexelés nem létezik.
	//tomb[-1] = 200;		// ez sor fordítási hibát okoz


	// Az index megadása lehet egy másik változó vagy kifejezés segítségével.
	int idx = 4;
	tomb[idx] = 21;

	tomb[2*idx-1] = 1;	// => tomb[7] = 1;


	///////////////////////////
	// elemek elérése ciklussal
	///////////////////////////

	int tomb3[10], i;
	for (i=0; i<10; i++)
	{
		tomb3[i] = i*i;
	}


	/////////
	// string
	/////////
	// Szövegábrázolás karaktertömb segítségével.
	// A karaktersorozat végét egy speciális karakter jelzi: '\0'.

	char text[10] = {'a', 'l', 'm', 'a', '\0'};
	printf("%s\n", text);

	scanf("%s", text);			// ide nem kell & jel.
	printf("%s\n", text);


	return 0;
}