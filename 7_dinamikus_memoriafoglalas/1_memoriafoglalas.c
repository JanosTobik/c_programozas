/*
============================================================================
Name        : 7_dinamikus_memoriafoglalas/1_memoriafoglalas.c
Author      : Tobik János
Version     : 1.0
Title		: Dinamikus memóriafoglalás
Description	: Program futása közben lehetőségünk van memóriát foglalni változóink részére. 
			  Ehhez szükség van a 'stdlib.h' könyvtárban található 'malloc()' függvényre.
			  A függvénynek paraméterként adhatjuk meg, hogy mekkora memóriaterületre van szükségünk.
			  Fontos, hogy a legfoglalt memóriát mindig szabadítsuk fel használat után.
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>


int main()
{

	// Szintaktika: típuskonverzió malloc( típus mérete )
	int *ptr = (int*)malloc(sizeof(int));

	*ptr = 123;

	printf("Pointer erteke: %d\n", *ptr);

	// Felszabadítás
	free(ptr);

	
/*-----------------------------------------------------------------------------------------------------------------------------------*/


	///////////////
	// Tömbfoglalás
	///////////////

	int size = 5;
	int *array = (int*) malloc(sizeof(int) * size);

	array[0] = 30;
	array[size-1] = 13;

	printf("array[0] és array[4] erteke: %d %d\n", array[0], array[4]);

	free(array);

	return 0;
}