/*
============================================================================
Name        : 7_dinamikus_memoriafoglalas/3_valtozo_meret.c
Author      : Tobik János
Version     : 1.0
Title		: Futás közben változó méret
Description	: Ha nem tudjuk előre mekkora méretre lesz szükségünk.
			  Lehetőségek:
			  	- nagy helyet foglalunk, biztos elég lesz
			  	- kis helyet foglalunk, és ha több kell foglalunk még
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>


int main()
{

	int i, size = 5;

	// Eredetileg foglalt memória
	int *old = (int*)malloc(sizeof(int) * size);

	// Új memóriafoglalás
	int *new = (int*)malloc(sizeof(int) * (size+1));

	// Elemeke másolása az új memóriaterületre.
	for (i=0; i<size; i++)
	{
		new[i] = old[i];
	}
	size++;

	// Régi terület felszabadítása
	free(old);

	// Pointer átállítása
	old = new;
	//new = NULL;	// nem szükséges


	// Felszabadítás használat után 
	free(old);



	return 0;
}