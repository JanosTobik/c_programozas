/*
============================================================================
Name        : 7_dinamikus_memoriafoglalas/2_matrixok.c
Author      : Tobik János
Version     : 1.0
Title		: Mátrixok
Description	: Mátrix: két dimenziós tömb.
			  Sorokból és oszlopokból épül fel.
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>


int main()
{

	//////////////////
	// Egyszerű mátrix
	//////////////////

	int matrix1[5][5];

	matrix1[0][3] = 14;
	matrix1[4][2] = 3;
	matrix1[1][1] = 81;

	printf("0. sor 3. oszlop: %d\n", matrix1[0][3]);
	printf("4. sor 2. oszlop: %d\n", matrix1[4][2]);
	printf("1. sor 1. oszlop: %d\n", matrix1[1][1]);


/*-----------------------------------------------------------------------------------------------------------------------------------*/


	/////////////////////
	// Dinamikus mátrixok
	/////////////////////

	int i, row = 3, col = 7;
	int **matrix = (int**)malloc(sizeof(int*) * row);

	for (i=0; i<row; i++)
	{
		matrix[i] = (int*)malloc(sizeof(int) * col);
	}

	matrix[2][4] = 51;

	printf("matrix[2][4] erteke: %d\n", matrix[2][4]);


	// Felszabadítás
	// Fontos a sorrend.
	// Először a sorokhoz tartozó oszlopokat kell felszabadítani és csak utána a sorokat.
	for (i=0; i<row; i++)
	{
		free(matrix[i]);
	}
	free(matrix);

	return 0;
}