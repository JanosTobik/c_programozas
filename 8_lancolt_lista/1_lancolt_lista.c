/*
============================================================================
Name        : 8_lancolt_lista/1_lancolt_lista.c
Author      : Tobik János
Version     : 1.0
Title		: Láncolt lista
Description	: Adattárolási struktúra, hasonló a tömbhöz.
			  Az elemek nem egymás után helyezkednek el a memóriában.
			  Ehelyett az aktuális elem pointerrel hivatkozik a következő elemre.

	         head           first           second               
              |               |               |                              
	      +---+----+      +---+----+      +---+----+     
          | 0 | n* |----->| 1 | n* |----->| 2 | n* |   
          +---+----+      +---+----+      +---+----+
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>

// Struktúra az egyes csomópontok kezelésére
typedef struct Node {
	int data;
	struct Node *next;
} Node;


int main()
{

	// A head csomópontot tekintjük az első elemnek.
	// Tekinthetjük olyan csomópontnak is, amely nem tartalmaz adatot.
	Node *head = (Node*)malloc(sizeof(Node));

	// További elemek létrehozása
	Node *element1 = (Node*)malloc(sizeof(Node));
	Node *element2 = (Node*)malloc(sizeof(Node));


	// Adatok és pointerek beállítása.
	head->data = 0;
	head->next = element1;

	element1->data = 1;
	element1->next = element2;

	element2->data = 2;
	element2->next = NULL;


/*-----------------------------------------------------------------------------------------------------------------------------------*/


	/////////////////
	// Elemek elérése
	/////////////////

	// Végigiterálás a láncolt listán és az adatok kiiratása.
	Node *iterator = head;
	while (iterator != NULL)
	{
		printf("Data: %d\n", iterator->data);
		iterator = iterator->next;	// Következő elem beállítása.
	}


/*-----------------------------------------------------------------------------------------------------------------------------------*/


	////////////////
	// Felszabadítás
	////////////////

	iterator = head;
	Node *temp;
	while (iterator != NULL)
	{
		temp = iterator;			// Aktuális elem elmentése, hogy később felszabadítsuk.
		iterator = iterator->next;	// Következő elem beállítása.
		free(temp);					// Felszabadítás.
	}


	return 0;
}
