/*
============================================================================
Name        : 8_lancolt_lista/2_ketiranyu_lista.c
Author      : Tobik János
Version     : 1.0
Title		: Kétirányú láncolt lista
Description	: Adattárolási struktúra, hasonló a tömbhöz.
			  Az elemek nem egymás után helyezkednek el a memóriában.
			  Ehelyett az aktuális elem pointerrel hivatkozik a következő elemre.

	         first                   second                  third     
               |                       |                       |                    
	      +----+------+----+      +----+------+----+      +----+------+----+              
      +-->| p* | data | n* |<---->| p* | data | n* |<---->| p* | data | n* |<--+              
      |   +----+------+----+      +----+------+----+      +----+------+----+   |          
      |                                                                        | 
      |                                                                        |    
      |                              head                                      |    
      |                               |                                        |   
      |                          +----+--------+----+                          |        
      +------------------------->| p* | (data) | n* |<-------------------------+        
                                 +----+--------+----+                                   
                                                                      
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>

// Struktúra az egyes csomópontok kezelésére
typedef struct Node {
	int data;
	struct Node *next;
	struct Node *prev;
} Node;


/////////////////////////
// Függvények deklarálása
/////////////////////////

void insertLast(Node *head, int data);
void insertFirst(Node *head, int data);
void deleteNode(Node *node);

int main()
{

	Node *head = (Node*)malloc(sizeof(Node));

	
	//////////////////////
	// Új elemek beszúrása
	//////////////////////

	insertLast(head, 1);
	insertLast(head, 2);
	insertLast(head, 3);
	insertLast(head, 4);

	insertFirst(head, 1);
	insertFirst(head, 2);
	insertFirst(head, 3);
	insertFirst(head, 4);

	///////////
	// Iterálás
	///////////

	// Specifikus elem törlése
	Node *iterator = head->next;
	while (iterator != head)
	{
		if (iterator->data == 3)
			deleteNode(iterator);

		iterator = iterator->next;	// Következő elem beállítása.
	}


	// Adatok kiírása
	iterator = head->next;
	while (iterator != head)
	{
		printf("Data: %d\n", iterator->data);
		if (iterator->data == 3)
			deleteNode(iterator);
		iterator = iterator->next;	// Következő elem beállítása.
	}



	////////////////
	// Felszabadítás
	////////////////

	iterator = head->next;

	while (iterator != head)
	{
		Node *temp = iterator->next;
		free(iterator);
		iterator = temp;
	}
	free(head);

	return 0;
}


void insertLast(Node *head, int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->data = data;		// adat beállítása az új nodenak

	if (head->prev == NULL)		// nincs még elem a listában
	{
		newNode->next = head;			// új node nextje legyen a head
		newNode->prev = head;			// új node prevje legyen a head
		head->next = newNode;			// head nextje legyen az új node
		head->prev = newNode;			// head prevje legyen az új node
	}
	else						// van már elem a listában
	{
		head->prev->next = newNode;		// régi utolsó nextje legyen az új node
		newNode->prev = head->prev;		// új node prevje legyen a régi utolsó
		newNode->next = head;			// új node nextje legyen a head
		head->prev = newNode;			// head prevje legyen az új node
	}
}

void insertFirst(Node *head, int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->data = data;		// adat beállítása az új nodenak

	if (head->next == NULL)		// nincs még elem a listában
	{
		newNode->next = head;			// új node nextje legyen a head
		newNode->prev = head;			// új node prevje legyen a head
		head->next = newNode;			// head nextje legyen az új node
		head->prev = newNode;			// head prevje legyen az új node
	}
	else						// van már elem a listában
	{
		head->next->prev = newNode;		// régi első prevje legyen az új node
		newNode->next = head->next;		// új node nextje legyen a head nextje
		newNode->prev = head;			// új node prevje legyen a head
		head->next = newNode;			// head nextje legyen az új node
	}
}

void deleteNode(Node *node)
{
	node->next->prev = node->prev;		// következő prevje legyen az előző nextje
	node->prev->next = node->next;		// előző nextje legyen a következő

	free(node);
}
