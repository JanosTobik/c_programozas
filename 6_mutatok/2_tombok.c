/*
============================================================================
Name        : 6_mutatok/2_tombok.c
Author      : Tobik János
Version     : 1.0
Title		: Mutató mint tömb
Description	: A tömbök hasonlóan viselkednek a mutatókhoz.
			  Ha a tömböt nem indexeljük, akkor az első elemének memóriacímét kapjuk.
			  A mutató is használható tömbként, ugyanúgy lehet indexelni.
============================================================================
*/

#include <stdio.h>

int main()
{
	
	/////////////
	// Létrehozás
	/////////////

	int tomb[10] = { 4, 3, 5, 2, 6, 4, 3, 36, 65, 2 };

	int *ptr = tomb;	// nem kell a címképző operátor

	ptr[1] = 80;

	printf("%d\n", tomb[1]);

	/*-----------------------------------------------------------------------------------------------------------------------------------*/

	// Ha a mutató értékét növeljük, akkor a növelés mértékét megszorozza a típusának méretével.

	printf("%p = %p\n", &tomb[3], ptr + 3);

	printf("%p = %p\n", tomb + 3, ptr + 3);
	
	printf("%d = %d\n", tomb[3], *(ptr + 3));

	printf("%d = %d\n", ptr[3], *(ptr + 3));


	int *ptr2 = ptr + 5;
	*ptr2 = 100;
	printf("tomb[5]: %d\n", tomb[5]);

	ptr2 = tomb + 6;
	*ptr2 = 130;
	printf("tomb[6]: %d\n", tomb[6]);
	

	return 0;
}