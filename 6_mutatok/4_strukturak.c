/*
============================================================================
Name        : 6_mutatok/4_strukturak.c
Author      : Tobik János
Version     : 1.0
Title		: Struktúra típusú mutató és mutató adattag struktúrában
Description	: Struktúra típusú változó címét is le tudjuk kérni.
			  Valamint struktúrában is lehet tárolni memóriacímeket.
============================================================================
*/

#include <stdio.h>

int main()
{
	
	/////////////
	// Létrehozás
	/////////////

	typedef struct cpu {
		char *name;
		float clock;
	} CPU;

	typedef struct computer {
		CPU *cpu;
		int id;
	} Computer;


	CPU myCpu;
	myCpu.name = "Intel";
	myCpu.clock = 2.8;

	Computer myComputer;
	myComputer.cpu = &myCpu;
	myComputer.id = 1;


	////////////
	// Tagelérés
	////////////
	// Mutatón keresztüli adattag elérése a '->' operátorral
	printf("myComputer\n");
	printf("CPU neve: %s es orajele: %.2f\n", myComputer.cpu->name, myComputer.cpu->clock);
	printf("Id: %d\n", myComputer.id);


	return 0;
}