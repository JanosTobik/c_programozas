/*
============================================================================
Name        : 6_mutatok/5_fuggvenyek.c
Author      : Tobik János
Version     : 1.0
Title		: Mutatók használata függvényparaméterként
Description	: Hasznos lehet ha a függvényen kívüli változót módosítani szeretnénk.
			  Fontos, hogy a függvény soha ne adjon vissza olyan mutatót, amely a függvényben létrehozott változóra mutat.
============================================================================
*/

#include <stdio.h>


void add(int *a, int b)
{
	*a = *a + b;
}


void swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}


int main()
{
	int a = 1;
	
	add(&a, 5);

	printf("a erteke: %d\n", a);




	int b = 10;

	swap(&a, &b);

	printf("a erteke: %d\n", a);
	printf("b erteke: %d\n", b);

	return 0;
}