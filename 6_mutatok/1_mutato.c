/*
============================================================================
Name        : 6_mutatok/1_mutato.c
Author      : Tobik János
Version     : 1.0
Title		: Mutatók
Description	: Minden változó egy memóriacímen található.
			  Nem csak a változó értékét, de a memóriacímét is elérhetjük.
============================================================================
*/

#include <stdio.h>

int main()
{
	
	/////////////////////
	// Mutató létrehozása
	/////////////////////
	int *p;
	int *q, *r;

	/*-----------------------------------------------------------------------------------------------------------------------------------*/

	/////////////
	// Operátorok
	/////////////
	// - Címképző operátor: &
	// - Indirekció operátor: *

	int x;
	
	//printf("Az 'x' erteke: %d\n", x);		// 'x' változó nincs inicializálva, fordítás közben Warning

	// A pointer mutasson az 'x' változó memóriacímére.
	p = &x;
	
	// A memóriacímen található érték módosítása.
	*p = 42;
	
	// 'x' változónak értéket adtunk a pointer segítségével.
	// 'p' arra a memóriacímre mutat, ahol 'x' is található, ezért változik annak értéke is.
	printf("Az 'x' erteke: %d\n", x);
	
	/*-----------------------------------------------------------------------------------------------------------------------------------*/	

	////////////
	// Értékadás
	////////////

	int i = 3;

	q = &i;		// 'q' az 'i' memóriacímét tárolja
	r = q;		// 'r' a 'q' memóriacímét tárolja

	*q = 50;
	*r = 8;

	printf("i erteke: %d\n", i);


	/*-----------------------------------------------------------------------------------------------------------------------------------*/	

	//////////////
	// Nullpointer
	//////////////
	// Ha a mutató érvénytelen címre mutat, nem szabad használni.
	// Maga a cím nem kérdezhető le, hogy érvényes-e.
	// Ha a mutató nem mutat sehova, mutasson a 0 címre (0, 0x0, NULL).

	int *nPtr = NULL;


	/*-----------------------------------------------------------------------------------------------------------------------------------*/	

	/////////////////////////
	// Mutatóra mutató mutató
	/////////////////////////

	int a = 6;
	int b = 2;
	int **pp;

	p = &a;
	pp = &p;

	*p = 11;
	**pp = 20;

	*pp = &b;
	**pp = 5;

	printf("a erteke: %d\n", a);
	printf("b erteke: %d\n", b);


	return 0;
}