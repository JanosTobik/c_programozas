/*
============================================================================
Name        : 6_mutatok/3_string.c
Author      : Tobik János
Version     : 1.0
Description : Mutatók: string
			  Szöveg reprezentálása, mint karakter tömb.
			  A sorozat vége egy speciális karakter: '\0'.
============================================================================
*/

#include <stdio.h>
#include <string.h>

int main()
{
	
	/////////////
	// Létrehozás
	/////////////

	char text[10] = { 'a', 'l', 'm', 'a', '\0' };
	char *p = text;

	printf("%s\n", p);
	scanf("%s", p);
	printf("Uj ertek: %s\n", p);


	/*-----------------------------------------------------------------------------------------------------------------------------------*/
	
	////////////////////
	// String függvények
	////////////////////
	// Szükség van a 'string.h' fájl importálására.
	// Számos string művelethez ajánlott függvényt tartalmaz.
	// A függvények itt találhatóak: http://www.cplusplus.com/reference/cstring/

	// 1. Összehasonlítás - strcmp()
	// Két szöveg között keresi a karakterkülönbségeket.
	// Az első talált különbségnél megáll.
	// Visszatérési értéke:
	//		- =0, ha a két szöveg megegyezik
	//		- >0, ha az első string karakterének értéke nagyobb mint a másodiké
	//		- <0, ha a második string karakterének értéke nagyobb mint az elsőé
	char *text1 = "egy";
	char *text2 = "ketto";

	printf("Eredmeny: %d\n", strcmp(text1, text2));
	printf("Eredmeny: %d\n", strcmp("szoveg1", "szoveg2"));


	// 2. Másolás - strcpy()
	// Egy forrás szöveg másolása a cél memóriaterületre.
	char cTomb[10];
	strcpy(cTomb, text1);

	printf("strcpy eredmenye: %s\n", cTomb);

	// 3. Konkatenáció - strcat
	char *text3 = strcat(cTomb, text2);
	printf("strcat eredmenye: %s\n", text3);

	return 0;
}