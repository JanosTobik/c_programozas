/*
============================================================================
Name        : 5_fuggvenyek/2_rekurzio.c
Author      : Tobik János
Version     : 1.0
Title		: Rekurzív függvények
Description	: Rekurzív függvény olyan függvény, amely tartalmaz önmagára való hivatkozást.
			  Másképpen a függvény meghívja saját magát.
============================================================================
*/

#include <stdio.h>

///////////////////////
// Faktoriális számítás
///////////////////////

int factorial(int number)
{
	if (number >= 1)
	{
		return number * factorial(number-1);
	}
	else
	{
		return 1;
	}
}



int main()
{
	int myNumber;
	
	scanf("%d", &myNumber);
	
	printf("%d\n", factorial(myNumber));

	return 0;
}
