/*
============================================================================
Name        : 5_fuggvenyek/1_fuggveny.c
Author      : Tobik János
Version     : 1.0
Title		: Függvények
Description	: A gyakran használt kódrészleteket függvényekbe rendszerezzük.
			  Ezáltal a kódbázis csökkenhet és átláthatóbb a megírt kódunk.
			  Külső könyvtárak használatára van lehetőségünk, mint például a 'stdio.h', amely többek között tartalmazza a 'printf()' és 'scanf()' függvényeket.
			  A függvények a működésükhöz kaphatnak paramétereket és vissza is adhatnak egy értéket.
			  Szintaktika:
			  	- Deklaráció: név, visszatérési érték, és paraméterezés definiálása
			  	- Definíció: működés meghatározása
============================================================================
*/

#include <stdio.h>

/////////////////////////////////////
// Példa: tömb kiiratása (deklaráció)
/////////////////////////////////////
void printArray(int tomb[], int size);		// definíció a 'main()' után

/*-----------------------------------------------------------------------------------------------------------------------------------*/

//////////////
// Paraméterek
//////////////
// A függvény érték szerint kapja meg a változókat mint paraméter és nem referencia szerint.
// Ez azt jelenti, hogy a függvényen belül a változón alkalmazott módosítások csak a függvényen belül érvényesek.
int add(int a, int b) {
	a++;
	return a+b;
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/

//////////////////
// !!!FONTOS!!! //
//////////////////
// Tömbök esetén a függvényben végzett módosítások a paraméterként kapott tömbre is érvényesek lesznek.
void zero(int tomb[], int size) {
	int i;
	for (i=0; i<size; i++)
	{
		tomb[i] = 0;
	}
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/



int main()
{
	int myArray[10] = { 4, 3, 5, 2, 6, 4, 3, 36, 65, 2 };
	
	// függvény meghívása
	printArray(myArray, 10);
	
	int a = 5;
	int b = 6;
	int c = add(a, b);
	printf("a erteke: %d\n", a);	// a függvénybe növeltük az 'a' változó értékét, mégis 5 marad az értéke
	printf("c erteke: %d\n", c);


	// előző tömb kinullázása
	zero(myArray, 10);
	// tömb kiirató függvény meghívása újra
	printArray(myArray, 10);


	return 0;
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////
// Példa: tömb kiiratása (definíció)
////////////////////////////////////

void printArray(int tomb[], int size)
{
	int i;
	for (i=0; i<size; i++)
	{
		printf("%d ", tomb[i]);
	}
	printf("\n");
}
