/*
============================================================================
Name        : 3_programozasi_tetelek/2_megszamlalas.c
Author      : Tobik János
Version     : 1.0
Title		: Megszámlálás
Description	: Megoldás elve:
			  - Új változó, ami az eredményt tárolja.
			  - Végig kell iterálni a tömbbön.
			  - Ha a feltétel igaz, növelni kell a számlálót.
============================================================================
*/

#include <stdio.h>

int main()
{
	int i;
	int tomb[10] = { 4, 3, 5, 2, 6, 4, 3, 36, 65, 2 };
	int ertek = 2;
	int szamlalo = 0;	// találatok számának elmentése, kezdetben =0

	for (i=0; i<10; i++)
	{
		if (tomb[i] == ertek)
		{
			szamlalo++;
		}
	}

	printf("A számláló értéke: %d\n", szamlalo);

	

	return 0;
}