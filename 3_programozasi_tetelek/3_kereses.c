/*
============================================================================
Name        : 3_programozasi_tetelek/3_kereses.c
Author      : Tobik János
Version     : 1.0
Title		: Keresés
Description	: Megoldás elve:
			  - Végig kell iterálni a tömbbön.
			  - Ha megtaláltuk az elemet, nem kell tovább keresni.
			  - Ha a feltétel igaz, leállhat a keresés.
============================================================================
*/

#include <stdio.h>

int main()
{
	int i;
	int tomb[10] = { 4, 3, 5, 2, 6, 4, 3, 36, 65, 2 };
	int ertek = 2;

	i = 0;
	while (i < 10 && ertek != tomb[i])
	{
		i++;
	}

	if (i < 10)
	{
		printf("A keresett elem indexe: %d\n", i);		// Az index 0-tól kezdődik.
	}
	else
	{
		printf("Nincs ilyen elem\n");
	}

	
	/******************************
	*       minimum keresés       *
	*******************************/

	// Megoldás elve:
	// - Kezdetben a 0. elemet vesszük a legkisebbnek.
	// - Ha van nála kisebb, váloztatjuk a változó értékét.
	// - Maximum keresés hasonló módon implementálható.

	////////////////
	// index keresés
	////////////////
	
	int min_idx = 0;
	
	for (i=0; i<10; i++)
	{
		if (tomb[i] < tomb[min_idx])
		{
			min_idx = i;
		}
	}

	printf("A legkisebb elem indexe: %d es erteke: %d\n", min_idx, tomb[min_idx]);


	////////////////
	// érték keresés
	////////////////

	int min_ertek = tomb[0];	// Kezdetben az első elemet tekintjük a legkisebbnek.

	for (i=0; i<10; i++)
	{
		if (tomb[i] < min_ertek)
		{
			min_ertek = tomb[i];
		}
	}

	printf("A legkisebb elem erteke: %d\n", min_ertek);


	return 0;
}