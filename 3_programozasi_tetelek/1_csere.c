/*
============================================================================
Name        : 3_programozasi_tetelek/1_csere.c
Author      : Tobik János
Version     : 1.0
Title		: Változók értékeinek cseréje
Description	: 
============================================================================
*/

#include <stdio.h>

int main()
{
	
	////////////////////////////////
	// két változó értékének cseréje
	////////////////////////////////

	int a=5, b=8;

	int c = a;	// segéd változó az egyik érték elmentésére
	a = b;
	b = c;

	printf("Az a valtozo erteke: %d\n", a);
	printf("Az b valtozo erteke: %d\n", b);
	printf("\n");


	/////////////////////////////////
	// segédváltozó használata nélkül
	/////////////////////////////////

	// 1. megoldás
	int x=5, y=9;

	x=x^y;
	y=x^y;
	x=x^y;
	printf("Az x valtozo erteke: %d\n", x);
	printf("Az y valtozo erteke: %d\n", y);
	printf("\n");
	
	
	// 2. megoldás
	x=5, y=9;

	x = x+y;
	y = x-y;
	x = x-y;
	printf("Az x valtozo erteke: %d\n", x);
	printf("Az y valtozo erteke: %d\n", y);
	printf("\n");
	

	// 3. megoldás
	x=5, y=9;

	x = x*y;
	y = x/y;
	x = x/y;
	printf("Az x valtozo erteke: %d\n", x);
	printf("Az y valtozo erteke: %d\n", y);
	printf("\n");
	

	return 0;
}