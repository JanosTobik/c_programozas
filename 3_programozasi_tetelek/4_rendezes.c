/*
============================================================================
Name        : 3_programozasi_tetelek/4_rendezes.c
Author      : Tobik János
Version     : 1.0
Title		: Rendezés
Description	: Megoldás elve:
			  - Legkisebb elem keresése.
			  - Ezt az első elemmel cserélni.
			  - Legkisebb keresés a maradék közül.
			  - Ezt a másodiik elemmel cserélni.
			  - Így tovább a tömb végéig.
	 		  - Két ciklus:
				- külső: melyik helyre keressük a következő legkisebb elemet
				- belső: legkisebb elem keresése az adott lehetőségek közül
============================================================================
*/

#include <stdio.h>

int main()
{
	
	int i, j;	// ciklusváltozók
	int min_idx = 0;
	int tomb[10] = { 4, 3, 5, 2, 6, 4, 3, 36, 65, 2 };

	for (i=0; i<10-1; i++)						// ha az első 9 elem rendezett, akkor a 10. elem is jó, ezért elég N-1 iteráció
	{
		min_idx = i;							// mindig az aktuális 'első' elem a tipp

		for (j=i+1; j<10; j++)					// a maradék tömb az eddig megtaláltak után kezdődik
		{
			if (tomb[j] < tomb[min_idx])		// legkisebb elem keresése
			{
				min_idx = j;
			}
		}

		int seged = tomb[i];					// külső ciklus akutális elemének és a legkisebb elem cseréje
		tomb[i] = tomb[min_idx];
		tomb[min_idx] = seged;
	}


	for (i=0; i<10; i++)
	{
		printf("%d\n", tomb[i]);
	}
	

	/*
		FONTOS!
		Számos rendezési algoritmus létezik.
		Feladattól függ, hogy melyiket célszerű használni.
	*/

	return 0;
}