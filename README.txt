Terminálból való fordítás:
	gcc -Wall -o executable main.c
		gcc: fordító
		kapcsolók:
			-Wall: fordítási hibák megjelenítése
			-o: output fájlnév megadása, ez a futtatandó fájl
		main.c: forrásfájl, amelyet fordítani szeretnénk

Terminálból való futtatás:
	./executable

Memóriaszivárgás vizsgálata:
	valgrind ./executable