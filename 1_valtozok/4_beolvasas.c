/*
============================================================================
Name        : 6_mutatok/3_string.c
Author      : Tobik János
Version     : 1.0
Title		: Beolvasás változóba
Description	: Ehhez szintén szükségünk van a 'stdio.h' fájlra, amely tartalmazza a 'scanf' függvényt.
			  Ezzel lehet az inputról beolvasni.
			  Szintaxisa hasonló a 'printf'-hez.
			  A beolvasás valójában a memóriába történik és nem a változóba.
			  Ha a memóraiterületen egy változó található, akkor annak értéke módosul.
			  A változó memóriacímét a & jellel lehet lekérdezni.
============================================================================
*/

#include <stdio.h>

int main()
{

	int i;
	scanf("%d", &i);
	printf("Az i valtozo erteke: %d\n", i);

	//////////////////////////
	// több változó beolvasása
	//////////////////////////

	int x, y, z;
	char c;

	scanf("%d %d %d %c", &x, &y, &z, &c);
	// Az értékeket szóközzel vagy sortöréssel elválasztva kell megadni.

	printf("A valtozok erteke: %d %d %d %c\n", x, y, z, c);


	//////////////////
	// szövegillesztés
	//////////////////

	scanf("%d,%d:%d", &x, &y, &z);
	printf("A valtozok erteke: %d %d %d\n", x, y, z);
	// Csak a megfelelő bemenetet fogadja el.
	// Speciális karakterek (pl: '\n') használata nem javasolt, mert megzavarhatják a működést.

	return 0;
}