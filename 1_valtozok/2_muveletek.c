/*
============================================================================
Name        : 1_valtozok/2_muveletek.c
Author      : Tobik János
Version     : 1.0
Title		: Műveletek változókkal
Description : Egyszerű matematikai műveletek végrehajtása.
============================================================================
*/

int main()
{

	int x = 1, y = 2, z = 3;

	x = y;
	x = 3 + 4 * 5;
	x = ((y+z)*3-12)/4;
	x = 2 * x;
	x = y * x + z - x + 13;
	x = -y*y*y;
	x = y%3;
	x = y=z=3;
	
	x+=3;			// [= x=x+3]
	x-=5;			// [= x=x-5]
	x*=2;			// [= x=x*2]
	x/=4;			// [= x=x/4]
	x+=(y-z)*3;		// [= x=x+(y-z)*3]
	x+=y+z-53;		// [= x=x+(y+z-53)]
	x%=6;			// [= x=x%6]
	x++;			// [= x=x+1]
	x--;			// [= x=x-1]

	return 0;
}