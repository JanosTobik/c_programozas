/*
============================================================================
Name        : 1_valtozok/1_letrehozas.c
Author      : Tobik János
Version     : 1.0
Title		: Változó létrehozás
Description	: Változók deklarálása és iniciálizálása.
			  Főbb változó típusok bemutatása.
============================================================================
*/

int main()
{
	/******************************
	* deklarálás és inicializálás *
	*******************************/

	//////////////////////////
	// deklarálás (létrehozás)
	//////////////////////////
	int x;

	////////////////////////////////////////////////////////////////
	// deklarálás és inicializálás (létrehozás és kezdeti értékadás)
	////////////////////////////////////////////////////////////////
	int y = 1;

	int apple = x + y * 2

	int p, q=90, r=4, s;

	/***********************
	* főbb változó típusok *
	************************/

	char c;						// 8 bit
	short s;					// 16 bit
	int i;						// 32 bit
	float f;					// 32 bit
	double d;					// 64 bit
			
	long int li;				// 32/64 bit
	long long int lli;			// 64 bit
	long double ld;				// 80 bit
			
	unsigned char uc;			// 8 bit
	unsigned short us;			// 16 bit
	unsigned int ui;			// 32 bit

	return 0;
}