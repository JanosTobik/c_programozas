/*
============================================================================
Name        : 1_valtozok/3_kiiras.c
Author      : Tobik János
Version     : 1.0
Title		: Változó értékének kiiratása
Description : Ehhez szükségünk van a 'printf' függvényre, amelyet a 'stdio.h' tartalmaz.
			  Ez a fájl számos fontos elemet tartalmaz, amelyre szükségünk lehet.
============================================================================
*/

#include <stdio.h>

int main()
{
	/////////
	// szöveg
	/////////
	printf("Hello World!\n");

	///////////////////////
	// speciális karakterek
	///////////////////////
	printf("\tbekezdés");
	printf("\nsortörés\n");

	//////////////////////////////
	// változó értékének kiiratása
	//////////////////////////////
	int x = 100;
	int y = 34;

	printf("Az x valtozo erteke: %d", x);
	printf("\n");
	printf("Az y valtozo erteke: %d\n", y);

	///////////////////////////////
	// egyéb változó típusok esetén
	///////////////////////////////
	char karakter = 'h';
	unsigned int elojelnelkuli = 1;
	float lebegopontos = 2.7;
	double duplapontos = 4.3;
	int hexa = 20;

	printf("Karakter erteke: %c\n", karakter);
	printf("Elojel nelkuli egesz erteke: %u\n", elojelnelkuli);
	printf("Hexadecimalis szam erteke: %x\n", hexa);
	printf("Lebegopontos szam erteke: %f\n", lebegopontos);
	printf("Duplapontossagu szam erteke: %lf\n", duplapontos);

	//////////////////////
	// változó memóriacíme
	//////////////////////

	// A program futása előtt a teljes kód betöltődik a memóriába.
	// A változók helye nem változik futás közben.
	// Általában bájtokra van osztva a memória, de szükség szerint nagyobb egységekben is kezelhető.
	// A változók helyét a & jel segítségével kérdezhetjük le, ez adja meg a memóriacímet.
	// Ezt hexadecimálsi formában kapjuk meg.

	printf("Az x valtozo a %p cimen talalhato\n", &x);

	///////////////////
	// formázott kiírás
	///////////////////

	// Kiírás fix hosszú helyre, jobbra igazítva
	printf("%10d\n", x);
	// Kiírás fix hosszú helyre, balra igazítva
	printf("%-10d\n", x);
	// Kiírás fix hosszú helyre, elejét 0-val feltöltve
	printf("%010d\n", x);
	// Pozitív előjelet is írja ki
	printf("%+d\n", x);

	// Lebegőpontos szám normálalakban
	printf("%e\n", lebegopontos);
	// Lebegőpontos szám a jobbnak tűnő alakban
	printf("%g\n", lebegopontos);
	// Lebegőpontos szám fix tizedes jeggyel
	printf("%.6f\n", lebegopontos);

	///////////////////
	// kifejezés kiírás
	///////////////////

	printf("%d\n", 3*x);

	

	return 0;
}