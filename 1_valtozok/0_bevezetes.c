/*
============================================================================
Name        : 1_valtozok/0_bevezetes.c
Author      : Tobik János
Version     : 1.0
Title		: Bevezetés
Description	: Egyszerű C program fordítása és futtatása.
============================================================================
*/

#include <stdio.h>

// a main függvény a program belépési pontja, itt indul a program futása
int main()
{
	printf("Hello World!\n");

	return 0;
}