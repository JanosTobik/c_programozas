/*
============================================================================
Name        : 9_fajlkezeles/2_fajl_iras.c
Author      : Tobik János
Version     : 1.0
Title		: Fájl írása
Description	:                                                                       
============================================================================
*/

#include <stdio.h>

int main()
{
	
	//////////////////
	// Fájl megnyitása
	//////////////////

	FILE *myFile = fopen("output.txt", "wt");	// t: text fájl

	if (myFile == NULL)
	{
		printf("Hiba, a program kilep!\n");
		return 1;
	}


	fprintf(myFile, "Első sor\n");
	fprintf(myFile, "Második sor\n");
	fprintf(myFile, "Harmadik sor\n");

	int idx;
	for (idx=0; idx<5; idx++)
	{
		fprintf(myFile, "Index: %d\n", idx);
	}


	fclose(myFile);


	return 0;
}