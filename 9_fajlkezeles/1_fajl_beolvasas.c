/*
============================================================================
Name        : 9_fajlkezeles/1_fajl_beolvasas.c
Author      : Tobik János
Version     : 1.0
Title		: Fájl beolvasása
Description	:                                                                       
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{

	//////////////////
	// Fájl megnyitása
	//////////////////
	// 'fopen()' szintaxis: (http://www.cplusplus.com/reference/cstdio/fopen/)
	//		- fájlnév
	//		- mód:
	//			- r		read
	//			- w		write
	//			- a		append
	//			- r+	read/update
	//			- w+	write/update
	//			- a+	append/update

	FILE *myFile;
	myFile = fopen("input.txt", "r");

	if (myFile == NULL)
	{
		printf("Hiba, a program kilep!\n");
		return 1;
	}


	///////////////////////
	// Soronkénti beolvasás
	///////////////////////

	// Memória lefoglalása egy sornyi adatnak
	int *line = (int*)malloc(sizeof(int));

	// Iterált olvasás, amíg el nem éri a fájl végét.
	// EOF speciális konstans jelzi a fájl végét. (EOF == End Of File)
	while (fscanf(myFile, "%d\n", line) != EOF)
	{
		printf("%d\n", (*line));
	}

		
	////////////////////////////////////////
	// Fájl bezárása és memóriafelszabadítás
	////////////////////////////////////////

	fclose(myFile);

	free(line);

	return 0;
}