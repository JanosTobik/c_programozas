# C programozás

## Témakörök

1. Változók
2. Vezérlési szerkezetek
3. Programozási tételek
4. Struktúrák
5. Függvények
6. Mutatók
7. Dinamikus memóriafoglalás
8. Láncolt lista
9. Fájlkezelés
10. Hasznos dolgok

## Program fordítása terminálban

	gcc -Wall -o executable main.c

* gcc: fordító
* argumentumok:
	* Wall: fordítási hibák megjelenítése
	* o: output fájlnév megadása, ez a futtatandó fájl
	* main.c: forrásfájl, amelyet fordítani szeretnénk

## Program futtatása terminálban

	./executable

## Memóriaszivárgás vizsgálata linux környezeten:

	valgrind ./executable

Ehhez telepíteni kell a valgrind programot: 
	
	sudo apt-get install valgrind

