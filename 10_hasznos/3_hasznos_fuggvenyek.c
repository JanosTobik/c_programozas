/*
============================================================================
Name        : 10_hasznos/3_hasznos_fuggvenyek.c
Author      : Tobik János
Version     : 1.0
Title		: Hasznos függvények
Description	: Hasznos függvények a 'stdlib.h', 'math.h' és 'string.h' könyvtárakból.
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

int main()
{

	/*
	*	stdlib.h	(http://www.cplusplus.com/reference/cstdlib/)
	*/

	///////
	// ATOI
	///////
	// Stringként megadott számot konvertálja integer típusúvá.

	printf("atoi('73') = %d\n", atoi("73"));
	printf("2 * atoi('16') = %d\n", 2 * atoi("16"));
	

	////////////////
	// RAND és SRAND
	////////////////
	// 'rand()' szintaxis: rand() % skála
	// pl: 	- rand() % 100            => 0-tól 99-ig
	// 		- rand() % 100 + 20       => 20-tól 119-ig

	printf("rand 1 = %d\n", rand() % 100);
	printf("rand 2 = %d\n", rand() % 100);
	printf("rand 3 = %d\n", rand() % 100 + 1);
	printf("rand 4 = %d\n", rand() % 30 + 50);

	// A rand függvény mindig ugyanazokat a random számokat adja vissza.
	// Ha minden futás alkalmával más random számokra van szükség, akkor a 'srand()' függvényt is használni kell.
	// Ezzel inicializálunk egy random generátort.

	srand(time(NULL));

	printf("Ezek a random szamok minden futasnal mas eredmenyt adnak: \n");
	printf("rand 1 = %d\n", rand() % 100);
	printf("rand 2 = %d\n", rand() % 100);
	printf("rand 3 = %d\n", rand() % 100 + 1);
	printf("rand 4 = %d\n", rand() % 30 + 50);



	/*
	*	math.h	(http://www.cplusplus.com/reference/cmath/)
	*/

	////////////////////
	// POW (hatványozás)
	////////////////////
	// Szintaxis: pow(szám, hatvány)

	printf("Az 5 3. hatvanya: %d\n", (int)pow(5, 3));
	printf("Az 4 4. hatvanya: %d\n", (int)pow(4, 4));
	printf("Az 2 10. hatvanya: %d\n", (int)pow(2, 10));


	///////////////////
	// SQRT (gyökvonás)
	///////////////////
	// Visszatérési értéke 'double'.

	printf("%lf\n", sqrt(16.0));
	printf("%d\n", (int)sqrt(81));



	/*
	*	string.h	(http://www.cplusplus.com/reference/cstring/)
	*/

	/////////
	// STRTOK
	/////////
	// A függvény a kapott stringet darabolja fel, a megadott valamely karakterek alapján.

	char szoveg[] = "Ez egy szoveg, ami fel lesz darabolva.";

	char *pStr = strtok(szoveg, " ,.");

	while (pStr != NULL)
	{
		printf("%s\n", pStr);
		pStr = strtok(NULL, " ,.");
	}


	/////////
	// STRLEN
	/////////
	// Visszaadja a paraméterként átadott string hosszát.


	int len = strlen("Teszt szoveg");
	printf("A szoveg hossza: %d\n", len);


	

	return 0;
}