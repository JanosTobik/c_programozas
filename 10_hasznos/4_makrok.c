/*
============================================================================
Name        : 10_hasznos/4_makrok.c
Author      : Tobik János
Version     : 1.0
Title		: Makrók
Description	: C programok fordítási folyamata: Preprocessing -> Compilation -> Assemble -> Linking
			  A makrók feldolgozása nem a fordító feladata, hanem a Preprocessor végzi el.
			  Ez lényegében egy szövegalapú feldolgozása a fájloknak.
============================================================================
*/

#include <stdio.h>

//////////////////////
// Változó definiálása
//////////////////////
#define N 10
#define X 200


#define ENABLED 1


/////////////////////////
// Függvény implementáció
/////////////////////////
#define foo(X) (X+2)
#define min(X, Y) ((X) < (Y) ? (X) : (Y))


/////////////////
// String kezelés
/////////////////
#define print(a) printf("A valtozo neve: "#a" ; erteke: %d\n", a);

	
int main()
{

	/////
	// 1.
	/////

	printf("N erteke: %d\n", N);
	printf("X erteke: %d\n", X);

	int i;
	for (i=0; i<N; i++)
	{
		i+=2;
	}


	/////
	// 2.
	/////

#if ENABLED == 1
	printf("Ez a kodreszlet lefut ha az ENABLED erteke igaz!\n");
#else
	printf("Ez a kodreszlet nem fut le ha az ENABLED erteke igaz!\n");
#endif


	
	/////
	// 3.
	/////

#ifdef ENABLED
#undef ENABLED

#ifndef ENABLED
	printf("Ez a kodreszlet lefut ha az ENABLED nincs definialva!\n");
#endif // end of #if !ENABLED


#define ENABLED
#ifdef ENABLED
	printf("Ez a kodreszlet lefut ha az ENABLED definialva van!\n");
#endif // end of #if ENABLED

#endif // end of #ifdef ENABLED


	/////
	// 4.
	/////

	printf("foo(2) erteke: %d\n", foo(2));
	printf("min(5, 10) erteke: %d\n", min(5, 10));



	/////
	// 5.
	/////

	int myVariable = 5;
	print(myVariable);



	return 0;
}