/*
============================================================================
Name        : 10_hasznos/5_main_argumentumok.c
Author      : Tobik János
Version     : 1.0
Title		: Command line argumentumok
Description	: A program futtatásánál megadhatunk különféle argumentumokat, amelyek befolyásolhatják a működést.
			  Pl: ./5 elso masodik harmadik negyedik otodik hatodik
============================================================================
*/

#include <stdio.h>


// A 'main' függvény a program belépési pontja.
// Így itt kell lekezelni a kapott argumentumokat.
// A 'main' első paramétere az argumentumok száma, a második maguk az argumentumok.
// Az első argumentum mindig a program neve.
int main(int argc, char *argv[])
{

	printf("Argumentumok szama: %d\n", argc);

	if (argc > 5)
	{
		printf("Tul sok argumentum. A program leall!\n");
		return 1;
	}

	while ((*argv) != NULL)
	{
		printf("%s\n", (*argv++));
	}
	


	return 0;
}