/*
============================================================================
Name        : 10_hasznos/1_tipuskonverzio.c
Author      : Tobik János
Version     : 1.0
Title		: Típuskonverzió
Description	: Típuskonverzió alatt azt értjük, hogy a változót másik típusúvá változtatjuk.
============================================================================
*/

#include <stdio.h>

int main()
{

	////////////////////////////////////////
	// Implicit (automatikus) típuskonverzió
	////////////////////////////////////////
	// Compiler végzi el helyettünk.
	// Információveszteséget okozhat.


	int i = 10;
	char c = 'b';

	int x = i + c;
	printf("Az x erteke: %d\n", x);
	printf("Az x erteke az ASCII tablaban: %c\n", x);
	// x == 108, mert 'b' karakter értéke az ASCII tábla szerint 98.
	// A 'char' típus szám és karakter tárolásra is használható.


	float y = x + 2.3;		// 'x' változó implicit módon 'float' típusú lesz

	printf("Az y erteke: %.2f\n", y);
	
	
	
	//////////////////////////
	// Explicit típuskonverzió
	//////////////////////////

	int a = 5;
	int b= 2;

	printf("%d\n", a / b);		// A tört rész elveszik.

	printf("%f\n", (float)a / b);





	float f = 5.98;

	printf("Az f erteke: %d\n", (int)f);


	return 0;
}