/*
============================================================================
Name        : 10_hasznos/2_bit_operatorok.c
Author      : Tobik János
Version     : 1.0
Title		: Bit operátorok
Description	: 
============================================================================
*/

#include <stdio.h>

int main()
{

	//////////////////
	// Bit művelet: ÉS
	//////////////////
	// Ott lesz 1 bit, ahol a bitpárok közül mindkettő bit értéke 1.

	// 'unsigned char' típus segítségével hozunk létre előjel nélküli 8 bites változót
	unsigned char a = 2;	// == 00000010
	unsigned char b = 3;	// == 00000011

	printf("2 & 3 = %u\n", a & b);	// 00000010 & 00000011 => 00000010 == 2
	printf("\n");

	
	////////////////////
	// Bit művelet: VAGY
	////////////////////
	// Ott lesz 1 bit, ahol a bitpárok közül legalább az egyik bit értéke 1.

	printf("2 | 3 = %u\n", a | b); // 00000010 | 00000011 => 00000011 == 3
	printf("\n");


	///////////////////
	// Bit művelet: XOR
	///////////////////
	// Ott lesz 1 bit, ha a bitpárok közül vagy az egyik bit értéke 1, vagy a másiké.

	printf("2 ^ 3 = %u\n", a ^ b); // 00000010 ^ 00000011 => 00000001 == 1
	printf("\n");


	///////////////////////
	// Bit művelet: NEGÁLÁS
	///////////////////////

	printf("~2 = %u\n", (unsigned char)~a); // ~00000010 => 11111101 == 253
	printf("~3 = %u\n", (unsigned char)~b); // ~00000011 => 11111100 == 252
	printf("\n");


	///////////////////////////////
	// Bit művelet: SHIFTELÉS (BAL)
	///////////////////////////////

	a = a << 2;		// 00000010 << 2 => 00001000 == 8
	b = b << 3;		// 00000011 << 3 => 00011000 == 24

	printf("2 << 2 = %u\n", a);
	printf("3 << 3 = %u\n", b);
	printf("\n");


	////////////////////////////////
	// Bit művelet: SHIFTELÉS (JOBB)
	////////////////////////////////

	a = a >> 3;		// 00001000 >> 3 => 00000001 == 1
	b = b >> 1;		// 00011000 >> 1 => 00001100 == 12

	printf("8 >> 3 = %u\n", a);
	printf("24 >> 1 = %u\n", b);
	

	return 0;
}