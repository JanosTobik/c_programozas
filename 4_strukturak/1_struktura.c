/*
============================================================================
Name        : 4_strukturak/1_struktura.c
Author      : Tobik János
Version     : 1.0
Title		: Struktúrák
Description	: Az egymáshoz köthető adatok tárolása együtt.
			  Pl: egy számítógépnek tároljuk a CPU órajelet, memória  és háttértár méretét.
			  Egy struktúrával magasabb absztrakciós szintet köthetünk egyes változókhoz.
============================================================================
*/

#include <stdio.h>

int main()
{

	struct computer
	{
		float cpu;
		int memory;
		int hdd;
	};

	// Ezzel létrehoztunk egy új típust, a 'struct computer' típust.

	// új computer létrehozása
	struct computer comp1, comp2;

	// tömb létrehozása
	struct computer compTomb[10];

	// struktúra adattagjainak elérése
	comp1.cpu = 2.8;
	comp1.memory = 8;
	comp1.hdd = 750;

	printf("CPU: %.2f ;Memory: %d ;HDD: %d\n", comp1.cpu, comp1.memory, comp1.hdd);
	

	////////////////
	// egyszerűsítés
	////////////////

	// A typedef kulcsszóval átnevezhetünk létező típusokat.
	// Pl:
	// 		- typedef int Integer		=>	Integer a=5; <=> int a=5;
	// 		- typedef char Karakter		=>	Karakter c='X'; <=> char c='X';

	typedef struct Computer
	{
		float cpu;
		int memory;
		int hdd;
	} Computer;

	Computer newComp;


	//////////////////////
	// tömb a struktúrában
	//////////////////////

	struct struct1 {
		int x, y;
		int tomb[50];
	};

	struct struct1 s1;
	s1.tomb[10] = 100;

	printf("s1.tomb[10] erteke: %d\n", s1.tomb[10]);


	///////////////////////////
	// struktúra a struktúrában
	///////////////////////////

	struct struct2 {
		struct struct1 myStruct1;
		int p, q, r;
	};

	struct struct2 s2;
	s2.myStruct1.x = 5;
	s2.myStruct1.tomb[2] = 13;
	s2.p = 40;

	printf("s2.myStruct1.x erteke: %d\n", s2.myStruct1.x);
	printf("s2.myStruct1.tomb[2] erteke: %d\n", s2.myStruct1.tomb[2]);
	printf("s2.p erteke: %d\n", s2.p);

	return 0;
}